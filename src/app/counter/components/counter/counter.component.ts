import { Component } from "@angular/core";


@Component({
  selector: 'app-counter',
  template: `

<h3> {{counter}} </h3>

<button (click)="add(2)">+1</button>
<button (click)="reset()"> Reset </button>
<button (click)="sustration()">-1</button>
  `
})
export class CounterComponent{

  public counter: number = 25;

  add(value: number): void {
    this.counter += value;
  }
  sustration(): void {
    this.counter -= 1;
  }

  reset() {
    this.counter = 10;
  }

}
