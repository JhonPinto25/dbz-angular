import { Component } from '@angular/core';

@Component({
  selector: 'app-heroes-hero',
  templateUrl: './hero.component.html',
  styleUrl: './hero.component.css'
})
export class HeroComponent {

  public name: string = "ironman"
  public age: number = 50;


  get capitalizedName():string{
    return this.name.toUpperCase();
  }

  getDescription():string{

    return `${ this.name} - ${ this.age}`;
  }

  changeNameHero(){

    this.name = "Spiderman";

  }

  changeAgeHero(){

    this.age = 25 ;

  }
  resetForm(){
    this.name = 'ironman';
    this.age = 50;
  }



}
