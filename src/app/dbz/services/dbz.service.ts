import { Injectable } from '@angular/core';
import { Character } from '../interfaces/character.interface';

import { v4 as uuid } from 'uuid';

@Injectable({
  providedIn: 'root',
})
export class DbzService {
  constructor() {}

  public characters: Character[] = [
    {
      id: uuid(),
      name: 'Krilin',
      power: 1000,
    },
    {
      id: uuid(),
      name: 'Goku',
      power: 9500,
    },
  ];

  addCharacterMain(character: Character): void {
    //console.log('Main Page');
    //console.log(character);

    //Forma 1 de crear ese nuevo objeto agregandole el uuid
    /* const newCharacter : Character = {
      id: uuid(),
      name: character.name,
      power: character.power

    } */

    //Forma 2 de crear ese nuevo objeto agregandole el uuid
    const newCharacter: Character = { id: uuid(), ...character };

    this.characters.push(newCharacter);
  }

  // Forma de realizar el delete por medio del indice del arreglo, sin embargo, esto no es buena practica
  /* onDeleteMain(index: number): void {
    this.characters.splice(index, 1);
  } */

  deleteCharacterById(id: string): void {
    this.characters = this.characters.filter(
      (character) => character.id !== id
    );
  }
}
